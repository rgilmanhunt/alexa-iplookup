/**
 * App ID for the skill
 */
var APP_ID = undefined; //replace with 'amzn1.echo-sdk-ams.app.[your-unique-value-here]';

// ipinfo uses http not https
var http = require('http');

/**
 * The AlexaSkill Module that has the AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

/**
 * URL prefix
 */
var urlPrefix = 'http://ipinfo.io/';

/**
 * IpLookupSkill is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var IpLookupSkill = function() {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
IpLookupSkill.prototype = Object.create(AlexaSkill.prototype);
IpLookupSkill.prototype.constructor = IpLookupSkill;

IpLookupSkill.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("IpLookupSkill onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);

    // any session init logic would go here
};

IpLookupSkill.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("IpLookupSkill onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    getWelcomeResponse(response);
};

IpLookupSkill.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);

    // any session cleanup logic would go here
};

IpLookupSkill.prototype.intentHandlers = {

    "GetIpInfoIntent": function (intent, session, response) {
        handleIpInfoIntentRequest(intent, session, response);
    },
    "GetVersionIntent": function (intent, session, response) {
        handleVersionIntentRequest(intent, session, response);
    },
    "AMAZON.HelpIntent": function (intent, session, response) {
        var speechText = "With I P Lookup, you can get the hostname and hopefully the location of any I P 4 I P Address." +
            "For example, you can ask 'where is four dot four dot four dot four.' Who would you like me to look up?";
        var repromptText = "Do you need a new lookup?";
        var speechOutput = {
            speech: speechText,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        var repromptOutput = {
            speech: repromptText,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.ask(speechOutput, repromptOutput);
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = {
                speech: "Goodbye",
                type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = {
                speech: "Goodbye",
                type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.tell(speechOutput);
    }
};

/**
 * Function to handle the onLaunch skill behavior
 */

function getWelcomeResponse(response) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    var cardTitle = "IP Lookup";
    var repromptText = "What is the IP you would like me to lookup?";
    var speechText = "What is the IP you would like me to lookup?";
    var cardOutput = "";
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.

    var speechOutput = {
        speech: "<speak>" + speechText + "</speak>",
        type: AlexaSkill.speechOutputType.SSML
    };
    var repromptOutput = {
        speech: repromptText,
        type: AlexaSkill.speechOutputType.PLAIN_TEXT
    };
    response.askWithCard(speechOutput, repromptOutput, cardTitle, cardOutput);
}

/**
 * Gets a poster prepares the speech to reply to the user.
 */
function handleVersionIntentRequest(intent, session, response) {

    var prefixContent = "";
    var cardContent = "";
    var cardTitle = "";

    var speechOutput = {
        speech: "<speak>I am version zero dot zero dot two</speak>",
        type: AlexaSkill.speechOutputType.SSML
    };
    var repromptOutput = {
        speech: "Do you want to hear the version again?",
        type: AlexaSkill.speechOutputType.PLAIN_TEXT
    };
    response.tellWithCard(speechOutput, cardTitle, cardContent);
}

/**
 * Gets a poster prepares the speech to reply to the user.
 */
function handleIpInfoIntentRequest(intent, session, response) {

    var pieces=[];

    if ( intent.slots.One ) {
        pieces.push( intent.slots.One.value )
    }
    if ( intent.slots.Two ) {
        pieces.push( intent.slots.Two.value )
    }
    if ( intent.slots.Three ) {
        pieces.push( intent.slots.Three.value )
    }
    if ( intent.slots.Four ) {
        pieces.push( intent.slots.Four.value )
    }

    if( pieces.length !== 4 ) {
        raiseHeck("Your address, " + address + ", appears to be malformed.");
    }

    var address=pieces.join('.');

    getIpAddress(address, function (result) {
        console.log(result);
        var speechText = ''
        if(result.status==='success')  {
            var ip = result.ip;
            var hostname = (result.hostname.length) ? result.hostname : false;
            var city = ( result.city.length ) ? result.city : false;
            var region = (result.country.length) ? result.country : false;
            var org = (result.org.length) ? result.org : false;

            speechText = ip ;
            if( hostname) {
                speechText += " is also known as " + hostname;
            }
            if(city||region) {
                speechText += " it is located in ";
                if(city) {
                    speechText += city + " "
                }
                if(region) {
                    speechText += region;
                }
            }
            if(org) {
                "and it belongs to " + org;
            }
            speechText += ".";
        }
        else {
            speechText = result.message;
        }

        var cardTitle="IP Lookup";
        var speechOutput = {
            speech: "<speak>"+ speechText + "</speak>",
            type: AlexaSkill.speechOutputType.SSML
        };
        response.tellWithCard(speechOutput, cardTitle, speechText);
    });
}


function getIpAddress(address, eventCallback) {
    url = urlPrefix + address;
    console.log('lookup: ' + url);

    http.get(url, function(res) {
        var body = '';
        res.on('data', function(chunk) {
            body+=chunk;
        });
        res.on('end', function () {
            try {
                var jsonResult = JSON.parse(body);
                jsonResult.status='success';
                eventCallback( jsonResult );
            }
            catch(err) {
                eventCallback(
                    {
                        status: 'error',
                        message: body
                    }
                )
            }
        })
    }).on('error', function (e) {
        console.log("Got error: ", e);
        raiseHeck("Sorry, there was some error fetching the address " + address);
    });
}

function raiseHeck(errString) {
        var speechText =   "I am sorry, there was an error: " + errString;
        var speechOutput = {
            speech: "<speak>"+ speechText + "</speak>",
            type: AlexaSkill.speechOutputType.SSML
        };
        var repromptOutput = {
            speech: repromptText,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };
        response.tellWithCard(speechOutput, "Error", speechText);
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the HistoryBuff Skill.
    var skill = new IpLookupSkill();
    skill.execute(event, context);
};

