#IP Lookup AWS Lambda

## Concepts

This is an Alexa lambda to use ipinfo.io's public API to get some IP information. It's not using an API key because I'm expecting requests to remain in the
free tier- under 1000 (as of today's writing). This also means that we must use http calls and not https calls.

## Example user interactions:
  User:  "Alexa, ask IpLookup who is 127.0.0.1"
  Alexa: "The hostname for 127.0.0.1 is localhost dot local. Goodbye."

## Setting up an alexa skill is outside the scope of this document.